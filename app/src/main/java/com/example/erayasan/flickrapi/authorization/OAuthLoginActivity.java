
package com.example.erayasan.flickrapi.authorization;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import com.codepath.utils.GenericsUtil;

public abstract class OAuthLoginActivity<T extends OAuthBaseClient> extends Activity implements OAuthBaseClient.OAuthAccessHandler {
    private T client;

    public OAuthLoginActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Class clientClass = this.getClientClass();
        Uri uri = this.getIntent().getData();

        try {
            this.client = (T)clientClass.getConstructor(new Class[]{Context.class}).newInstance(new Object[]{this});
            this.client.authorize(uri, this);
        } catch (Exception var5) {
            var5.printStackTrace();
        }

    }

    public T getClient() {
        return this.client;
    }

    private Class<T> getClientClass() {
        return (Class)GenericsUtil.getTypeArguments(OAuthLoginActivity.class, this.getClass()).get(0);
    }
}
