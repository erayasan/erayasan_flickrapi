package com.example.erayasan.flickrapi.tasks;

/**
 * Created by erayasan on 22/06/16.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.example.erayasan.flickrapi.Photo;
import com.example.erayasan.flickrapi.activity.MainActivity;
import com.example.erayasan.flickrapi.utils.Config;

import java.io.InputStream;
import java.net.URL;

public class DownloadImageTask extends AsyncTask<Photo, Void, Bitmap> {
    private static final String LOG_E_TAG = "DownloadImageTask";
    private AsyncResponse response=null;

    public interface AsyncResponse {
        void onProcessFinished(Bitmap bm);
        void fireOnEveryTen();
    }

    public DownloadImageTask(AsyncResponse response) {
        this.response = response;
    }

    @Override
    protected Bitmap doInBackground(Photo... photos) {
        URL imageURL;
        Bitmap downloadedBitmap=null;
        InputStream inputStream;

        try {
            if(photos.length==1) {
                imageURL = new URL(photos[0].getImageUrl(Config.IMAGE_SIZE_LARGE_SQUARE));
                inputStream = imageURL.openStream();
                downloadedBitmap = BitmapFactory.decodeStream(inputStream);
                MainActivity.photoCache.put(photos[0].getBitmapKey(),downloadedBitmap);
            }
            else{
                for(int i=0; i<photos.length; i++){
                    imageURL = new URL(photos[i].getImageUrl(Config.IMAGE_SIZE_LARGE_SQUARE));
                    inputStream = imageURL.openStream();
                    downloadedBitmap = BitmapFactory.decodeStream(inputStream);
                    MainActivity.photoCache.put(photos[i].getBitmapKey(),downloadedBitmap);
                    if((i+1)%10==0)
                        response.fireOnEveryTen();
                }
            }

        } catch (Exception e) {
            downloadedBitmap = null;
            Log.e(LOG_E_TAG, e.getMessage());
            e.printStackTrace();
        }
        Log.d(LOG_E_TAG, "do in background finished");
        return downloadedBitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
            response.onProcessFinished(result);
    }
}