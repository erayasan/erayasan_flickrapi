package com.example.erayasan.flickrapi.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.erayasan.flickrapi.Photo;
import com.example.erayasan.flickrapi.R;
import com.example.erayasan.flickrapi.activity.MainActivity;
import com.example.erayasan.flickrapi.utils.Config;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by erayasan on 27/06/16.
 */
public class SimplePagerAdapter extends PagerAdapter {

    private Activity activity;
    private int screenWidth;
    private LinearLayout topDetailsLayout;
    private TextView titleDetailTextView;
    private TextView usernameDateDetailTextView;
    private TextView locationDetailTextView;
    private TextView bottomDetailsTextView;
    private static final int SWIPE_DISTANCE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    @Override
    public int getCount() {
        return MainActivity.photosList.size();
    }

    public SimplePagerAdapter(Activity activity, int screenWidth) {
        this.activity = activity;
        this.screenWidth = screenWidth;
        titleDetailTextView = (TextView) activity.findViewById(R.id.titleDetail);
        usernameDateDetailTextView = (TextView) activity.findViewById(R.id.usernameDateDetail);
        locationDetailTextView = (TextView) activity.findViewById(R.id.locationDetail);
        topDetailsLayout = (LinearLayout)activity.findViewById(R.id.topDetailsLayout);
        bottomDetailsTextView = (TextView)activity.findViewById(R.id.bottomPhotoDetail);
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {
        final PhotoView photoView = new PhotoView(container.getContext());
        Photo currentPhoto = MainActivity.photosList.get(position);

        Glide.with(activity)
                .load(currentPhoto.getImageUrl(Config.IMAGE_SIZE_LARGE))
                .override(screenWidth,1500)
                .error(R.drawable.loading_error)
                .placeholder(R.drawable.loading)
                .into(photoView);

        titleDetailTextView.setText(currentPhoto.getTitle());
        usernameDateDetailTextView.setText(currentPhoto.getOwner() + ", " + currentPhoto.getDate());
        locationDetailTextView.setText(currentPhoto.getLocation());
        bottomDetailsTextView.setText(currentPhoto.getDescription());

        // Now just add PhotoView to ViewPager and return it
        container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
photoView.setOnLongClickListener(new View.OnLongClickListener() {
    @Override
    public boolean onLongClick(View view) {
        Toast.makeText(activity,"Long Clicked",Toast.LENGTH_SHORT).show();
        return false;
    }
});
        photoView.setOnSingleFlingListener(new PhotoViewAttacher.OnSingleFlingListener() {
            @Override
            public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float velocityX, float velocityY) {
                //Toast.makeText(activity, "Fling", Toast.LENGTH_SHORT).show();

                float distanceX = motionEvent1.getX() - motionEvent.getX();
                float distanceY = motionEvent1.getY() - motionEvent.getY();



                if (Math.abs(distanceY) > Math.abs(distanceX) && Math.abs(distanceY) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (distanceY > 0) // swipe up
                        fadeAnimation(photoView,600,true,true);
                    else // swipe down
                        fadeAnimation(photoView,600,true,false);
                    return true;
                }
                return false;

            }
        });
        photoView.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
            @Override
            public void onViewTap(View view, float v, float v1) {

                if(topDetailsLayout.getVisibility()==View.VISIBLE){
                    fadeAnimation(topDetailsLayout,250,true,false);
                    fadeAnimation(bottomDetailsTextView,250,true,false);
                }else{
                    fadeAnimation(topDetailsLayout,250,false,false);
                    fadeAnimation(bottomDetailsTextView,250,false,false);
                }
            }
        });
        return photoView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private void fadeAnimation(final View view, final int durationMs, final boolean hide,boolean zoomIn)
    {
        AnimationSet animationSet = new AnimationSet(true);

        Animation fade;
        if(hide)
            fade = new AlphaAnimation(1, 0);
        else
            fade = new AlphaAnimation(0,1);

        fade.setInterpolator(new AccelerateInterpolator());
   //     fade.setDuration(durationMs);
        float scaleFrom = 0.0f;
        float scaleTo = 0.0f;
        if(zoomIn) {
            scaleFrom = 0.3f;
            scaleTo = 0.5f;
        }
        else{
            scaleFrom = 2.5f;
            scaleTo = 0.5f;
        }
        Animation scale = new ScaleAnimation(view.getScaleX(),scaleFrom,view.getScaleY(),scaleFrom,Animation.RELATIVE_TO_SELF, scaleTo, Animation.RELATIVE_TO_SELF, scaleTo);
      //  scale.setDuration(durationMs);

        animationSet.addAnimation(fade);
        animationSet.setDuration(durationMs);

        if(view instanceof PhotoView)
            animationSet.addAnimation(scale);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(view instanceof PhotoView)
                    activity.finish();

                if(hide)
                    view.setVisibility(View.INVISIBLE);
                else
                    view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animationSet);

/*        fade.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationEnd(Animation animation)
            {
                if(view instanceof PhotoView)
                    activity.finish();

                if(hide)
                    view.setVisibility(View.GONE);
                else
                    view.setVisibility(View.VISIBLE);
               // activity.finish();
            }
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {


            }
        });
        view.startAnimation(fade);*/
    }

}