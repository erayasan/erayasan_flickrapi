package com.example.erayasan.flickrapi.activity;

/**
 * Created by erayasan on 30/06/16.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.example.erayasan.flickrapi.R;
import com.example.erayasan.flickrapi.authorization.FlickrClient;
import com.example.erayasan.flickrapi.authorization.OAuthAsyncHttpClient;
import com.example.erayasan.flickrapi.authorization.OAuthLoginActivity;
import com.example.erayasan.flickrapi.utils.Config;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FlickrApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


public class LoginActivity extends OAuthLoginActivity<FlickrClient> {
    TextView textView;
    WebView loginWebView;

    OAuthService service;
    SharedPreferences settings;
    static Token staticRequestToken;

    public static final String PROTECTED_RESOURCE_URL = "https://api.flickr.com/services/rest/";
    public static FlickrClient flickrClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        textView = (TextView)findViewById(R.id.textview);
        loginWebView = (WebView)findViewById(R.id.login_webView);

        WebSettings webSettings = loginWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        loginWebView.getSettings().setJavaScriptEnabled(true);
        loginWebView.getSettings().setDomStorageEnabled(true);
        loginWebView.getSettings().setPluginState(WebSettings.PluginState.ON);

        service = new ServiceBuilder()
                .provider(FlickrApi.class)
                .apiKey(Config.FLICKR_KEY)
                .apiSecret( Config.FLICKR_SECRET )
                .callback(FlickrClient.REST_CALLBACK_URL )
                .build();

        new WebViewAsyncTask().execute();

        loginWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if(url.startsWith(FlickrClient.REST_CALLBACK_URL) && staticRequestToken!=null)
                {
                    Uri uri = Uri.parse(url);
                    String verifier = uri.getQueryParameter("oauth_verifier");
                    Verifier v = new Verifier(verifier);

                    //save this token for practical use.
                    // TODO connection inside AsyncTask or use another library maybe ?
                    Token accessToken = service.getAccessToken(staticRequestToken, v);

                    OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.linkedin.com/v1/people/~:(first-name,last-name)");
                    service.signRequest(accessToken, request);
                    Response response = request.send();


                    settings = getSharedPreferences("preferences", 0);
                    SharedPreferences.Editor editor = settings.edit();

                    editor.putString("accessToken", accessToken.getToken());
                    Log.d("accessToken",accessToken.getToken());

                    // The requestToken is saved for use later on to verify the OAuth request.
                    // See onResume() below
                    editor.putString("requestToken", staticRequestToken.getToken());
                    editor.putString("requestSecret", staticRequestToken.getSecret());
                    editor.commit();

                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

        });


        Log.d("login","onCreate");

    }

    public class WebViewAsyncTask extends AsyncTask<Void, Void, Token> {


        @Override
        protected void onPreExecute() {
            // Here you can show progress bar or something on the similar lines.
            // Since you are in a UI thread here.
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Token requestToken) {
            super.onPostExecute(requestToken);
            textView.setText("Authorized!! On Post");
            final String authURL = service.getAuthorizationUrl(requestToken);
            Log.d("webViewDebug","tauthUrl: "+authURL);
            LoginActivity.staticRequestToken=requestToken;
            loginWebView.loadUrl(authURL);
        }

        @Override
        protected Token doInBackground(Void... params) {
            final Token requestToken = service.getRequestToken();
            Log.d("webViewDebug","token Receive"+requestToken.getToken());
            return requestToken;
        }
    }

    @Override
    public void onLoginSuccess() {
        Log.d("login","success");
       // new MyAsyncTask().execute();
        flickrClient= getClient();
       // String username = getValue(e, "username");
       // Intent intent = new Intent(LoginActivity.this, MainActivity.class);
       // intent.putExtra("username",username);
       // intent.putExtra("userid",userid);
      //  startActivity(intent);
        Toast.makeText(getApplicationContext(),"Authorized!!",Toast.LENGTH_LONG).show();

    }

    @Override
    public void onLoginFailure(Exception e) {
        Log.d("login","failure");
        e.printStackTrace();
    }

    public void loginToRest(View view) {
        Log.d("login","connecting..");
        getClient().connect();
    }

    public void startTestActivity(View view){
        Intent intent = new Intent(LoginActivity.this, TestActivity.class);
        startActivity(intent);
    }

    public void skipLogin(View view) {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("username","username");
        intent.putExtra("userid","userid");
        startActivity(intent);


    }


    public class MyAsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            // Here you can show progress bar or something on the similar lines.
            // Since you are in a UI thread here.
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            textView.setText("Authorized!! On Post");
            Document doc = getDomElement(aVoid); // getting DOM element
            NodeList nl = doc.getElementsByTagName("user");
            // looping through all item nodes <item>
                // creating new HashMap
                String userid = nl.item(0).getAttributes().getNamedItem("id").getNodeValue();
                Element e = (Element) nl.item(0);
                // adding each child node to HashMap key => value
                String username = getValue(e, "username");
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.putExtra("username",username);
                intent.putExtra("userid",userid);
                startActivity(intent);
        }

        @Override
        protected String doInBackground(Void... params) {
            OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
            request.addQuerystringParameter("method", "flickr.test.login");
            OAuthAsyncHttpClient.service.signRequest(getClient().checkAccessToken(), request);
            Response response = request.send();
            Log.d("response",response.getBody());
            Log.d("response",response.getMessage());
            Log.d("response",response.isSuccessful()+"");
            return response.getBody();
        }
    }

    public String getValue(Element item, String str) {
        NodeList n = item.getElementsByTagName(str);
        return this.getElementValue(n.item(0));
    }

    public final String getElementValue( Node elem ) {
        Node child;
        if( elem != null){
            if (elem.hasChildNodes()){
                for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() ){
                    if( child.getNodeType() == Node.TEXT_NODE  ){
                        return child.getNodeValue();
                    }
                }
            }
        }
        return "";
    }

    public Document getDomElement(String xml){
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        }
        // return DOM
        return doc;
    }

}
