package com.example.erayasan.flickrapi.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.example.erayasan.flickrapi.R;

public class WebActivity extends AppCompatActivity {
ProgressDialog pDialog;
    WebView myWebView;
    String authCode = "null_NULL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        myWebView = (WebView) findViewById(R.id.webView);
        pDialog = new ProgressDialog(WebActivity.this);
        initWebView();
    }

    public void initWebView(){
        myWebView.setWebViewClient(new WebViewClient() {

            boolean authComplete = false;
            Intent resultIntent = new Intent();

            @Override public void onPageStarted(WebView view, String url, Bitmap favicon){
                super.onPageStarted(view, url, favicon);
                pDialog = ProgressDialog.show(view.getContext(), "",
                        "Connecting to flickr server", false);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pDialog.dismiss();

                if (url.contains("?code=") && authComplete != true) {
                    Uri uri = Uri.parse(url);
                    authCode = uri.getQueryParameter("code");
                    Log.i("", "CODE : " + authCode);
                    authComplete = true;
                    resultIntent.putExtra("code", authCode);
                    WebActivity.this
                            .setResult(Activity.RESULT_OK, resultIntent);
                    resultIntent.putExtra("status", "success");
                    setResult(Activity.RESULT_CANCELED, resultIntent);
                    finish();
                }else if(url.contains("error=access_denied")){
                    Log.i("", "ACCESS_DENIED_HERE");
                    resultIntent.putExtra("code", authCode);
                    resultIntent.putExtra("status", "access_denied");
                    authComplete = true;
                    setResult(Activity.RESULT_CANCELED, resultIntent);
                    finish();
                }
            }
        });
    }
}
