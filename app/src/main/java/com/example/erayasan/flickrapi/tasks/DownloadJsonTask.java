package com.example.erayasan.flickrapi.tasks;

/**
 * Created by erayasan on 23/06/16.
 */

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadJsonTask extends AsyncTask<String, Void, JSONObject> {

    private static final String LOG_E_TAG = "DownloadJsonTask";
    private AsyncResponse response=null;

    public interface AsyncResponse {
        void onProcessFinished(JSONObject result);
    }

    public DownloadJsonTask(AsyncResponse response) {
        this.response = response;
    }

    @Override
    protected JSONObject doInBackground(String... url) {
        JSONObject jsonObject;
        try {
            jsonObject=getJSONObjectFromURL(url[0]);
        } catch (Exception e) {
            jsonObject = null;
            Log.e(LOG_E_TAG, e.getMessage());
            e.printStackTrace();
        }
        Log.d(LOG_E_TAG, "do in background finished");
        return jsonObject;
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        response.onProcessFinished(result);

    }


    private JSONObject getJSONObjectFromURL(String urlString) throws IOException, JSONException {

        HttpURLConnection urlConnection = null;

        URL url = new URL(urlString);

        urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(10000 /* milliseconds */);
        urlConnection.setConnectTimeout(15000 /* milliseconds */);

        urlConnection.setDoOutput(true);

        urlConnection.connect();

        BufferedReader br=new BufferedReader(new InputStreamReader(url.openStream()));

        char[] buffer = new char[1024];

        String jsonString;

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line+"\n");
        }
        br.close();

        jsonString = sb.toString();

      //  System.out.println("JSON: " + jsonString);

        return new JSONObject(jsonString);
    }
}
