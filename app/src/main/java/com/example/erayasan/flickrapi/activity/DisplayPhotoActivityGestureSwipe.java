package com.example.erayasan.flickrapi.activity;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.erayasan.flickrapi.R;
import com.example.erayasan.flickrapi.utils.Config;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class DisplayPhotoActivityGestureSwipe extends AppCompatActivity {

    ImageView fullscreenImageView;
    private static final String TAG = "DsplyPhotoActvtyGesture";
    private int currentPhotoPosition;
    PhotoViewAttacher mAttacher;
    PhotoView photoView;

    private static final int SWIPE_DISTANCE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;
    private int screenWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_photo);
        int itemClicked = getExtraInteger(savedInstanceState, "itemClicked");
        currentPhotoPosition = itemClicked;
        Log.d(TAG, itemClicked + "");

        WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;

        photoView = (PhotoView) findViewById(R.id.fullScreen_photoView);


        // fullscreenImageView = (ImageView)findViewById(R.id.fullscreen_imageview);
        mAttacher = new PhotoViewAttacher(photoView);
        mAttacher.setOnSingleFlingListener(new PhotoViewAttacher.OnSingleFlingListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                float distanceX = e2.getX() - e1.getX();
                float distanceY = e2.getY() - e1.getY();
                if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (distanceX > 0) // swipe right
                        setImageInPosition(--currentPhotoPosition);
                    else // swipe left
                        setImageInPosition(++currentPhotoPosition);
                    return true;
                }
                return false;
            }
        });
        setImageInPosition(itemClicked);

        // new DownloadImageTask(fullscreenImageView).execute(imageUrl);
    }

    private void setImageInPosition(int position) {
        if (position < 0) {
            position = 0;
            currentPhotoPosition = 0;
        } else if (position == MainActivity.photosList.size()) {
            position = MainActivity.photosList.size() - 1;
            currentPhotoPosition = position;
        }

        Glide.with(getApplicationContext())
                .load(MainActivity.photosList.get(position).getImageUrl(Config.IMAGE_SIZE_LARGE))
                .override(screenWidth,1500)
                .error(R.drawable.loading_error)
                .placeholder(R.drawable.loading)
                .into(photoView);
        //   mAttacher.update();
        //  fullscreenImageView.refreshDrawableState();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        return super.onTouchEvent(event);
    }

    public int getExtraInteger(Bundle savedInstanceState, String key) {
        int result;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                result = -1;
            } else {
                result = extras.getInt(key);
            }
        } else {
            result = (int) savedInstanceState.getSerializable(key);
        }
        return result;
    }
}
