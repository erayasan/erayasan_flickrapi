package com.example.erayasan.flickrapi.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.erayasan.flickrapi.Photo;
import com.example.erayasan.flickrapi.activity.MainActivity;
import com.example.erayasan.flickrapi.utils.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;

/**
 * Created by erayasan on 14/07/16.
 */
public class XMLParsePhotoDetailsTask extends AsyncTask<Void,Void,Void> {


    @Override
    protected Void doInBackground(Void... objects) {
        for(int i = 0; i< MainActivity.photosList.size(); i++){
            try{
                Photo currentPhoto = MainActivity.photosList.get(i);
                Log.d("XMLParser:" , "inside try");
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                String detail_url = String.format(Config.API_URL_PHOTO_DETAILS,Config.FETCH_IMAGE_DETAILS,Config.FLICKR_KEY,currentPhoto.getId(),currentPhoto.getSecret());
                URL url = new URL(detail_url);
                Log.d("detail_url",detail_url);
                InputStream stream = url.openStream();
                xpp.setInput(stream, null);
                int eventType = xpp.getEventType();

                while (eventType != XmlPullParser.END_DOCUMENT) {

                    if (eventType == XmlPullParser.START_DOCUMENT) {
                    }
                    else if (eventType == XmlPullParser.END_DOCUMENT) {
                    }
                    else if (eventType == XmlPullParser.START_TAG) {
                        String currentTag = xpp.getName();
                        if (currentTag.equalsIgnoreCase("title")){
                            String title = xpp.nextText();
                            MainActivity.photosList.get(i).setTitle(title);
                            //showIds.add(Integer.parseInt(xpp.nextText()));
                        }else if (currentTag.equalsIgnoreCase("description")){
                            String desc = xpp.nextText();
                            MainActivity.photosList.get(i).setDescription(desc);
                            //showIds.add(Integer.parseInt(xpp.nextText()));
                        }else if(currentTag.equalsIgnoreCase("owner")){
                            String owner = xpp.getAttributeValue(null,"username");
                            String location = xpp.getAttributeValue(null,"location");
                            MainActivity.photosList.get(i).setOwner(owner);
                            MainActivity.photosList.get(i).setLocation(location);
                        }else if(currentTag.equalsIgnoreCase("dates")){
                            String takenDate = xpp.getAttributeValue(null,"taken");
                            MainActivity.photosList.get(i).setDate(takenDate.substring(0,10));
                        }else if (currentTag.equalsIgnoreCase("url")){
                            String flickrUrl = xpp.nextText();
                            MainActivity.photosList.get(i).setFlickrUrl(flickrUrl);
                            //showIds.add(Integer.parseInt(xpp.nextText())); 17:25:34.606
                        }
                        else if (eventType == XmlPullParser.END_TAG) {
                        }
                        else if (eventType == XmlPullParser.TEXT) {
                            if(xpp.getName().equalsIgnoreCase("username")){
                                String username = xpp.nextText();
                                Log.d("XMLParser:" , "username tag found");
                                Log.d("XMLParser username: " , username);
                            }
                        }
                    }
                    eventType = xpp.next(); // <-- move here
                }



            }catch(Exception ex){

            }

        }
        return null;
    }

    public String getXmlFromUrl(String url) {
        String xml = null;

        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            xml = EntityUtils.toString(httpEntity);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // return XML
        return xml;
    }



}
