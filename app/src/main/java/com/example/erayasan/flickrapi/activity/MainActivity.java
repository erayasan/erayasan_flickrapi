package com.example.erayasan.flickrapi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.LruCache;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.erayasan.flickrapi.OnVerticalScrollListener;
import com.example.erayasan.flickrapi.Photo;
import com.example.erayasan.flickrapi.Place;
import com.example.erayasan.flickrapi.R;
import com.example.erayasan.flickrapi.adapter.CustomRecyclerViewAdapter;
import com.example.erayasan.flickrapi.adapter.GridLayoutAdapter;
import com.example.erayasan.flickrapi.tasks.DownloadJsonTask;
import com.example.erayasan.flickrapi.tasks.XMLParsePhotoDetailsTask;
import com.example.erayasan.flickrapi.utils.Config;
import com.example.erayasan.flickrapi.utils.CustomMaterialSearchView;
import com.example.erayasan.flickrapi.utils.ShakeDetector;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import br.com.mauker.materialsearchview.MaterialSearchView;


public class MainActivity extends CompactHeaderDrawerActivity implements SwipeRefreshLayout.OnRefreshListener{
    //static variables (lists,cache,etc..)
    public static ArrayList<Photo> photosList = new ArrayList<>();
    public static ArrayList<String> popularTags = new ArrayList<>();
    public static LruCache<Integer,Bitmap> photoCache = new LruCache<>(Config.MEMORY_CACHE_SIZE);
    public static AtomicInteger photoKeyGenerator = new AtomicInteger();
    public static SwipeRefreshLayout swipeRefreshLayout;
    //TAG for debug
    private final static String TAG = "MainActivity";
    // User interface
    private CustomMaterialSearchView searchView;
    private RecyclerView recyclerViewMain;
    private RecyclerView.LayoutManager mLayoutManager;
    private CustomRecyclerViewAdapter adapter;
    private ProgressDialog progressDialog;
    // Sensor elements for shake detection
    private ShakeDetector mShakeDetector;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    //application logic variables
    private int pages = 1;
    private String currentTag;
    private String currentLon;
    private String currentLat;
    private String currentUserName;
    private String currentUserId;

    ArrayAdapter<String> arrayAdapter;
    public ArrayList<Place> places;
    String oldSearchText = null;
    boolean firstRun = true;
    private String currentSearchType;
    private RelativeLayout bottomLayout;
    private boolean isLoadingNewItems;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_main, super.frameLayout);

       // toolbar = (Toolbar) findViewById(R.id.toolbar); setSupportActionBar(toolbar);
       // setSupportActionBar(super.toolbar);
        places = new ArrayList<>();
        searchView = (CustomMaterialSearchView)findViewById(R.id.search_view);
        recyclerViewMain = (RecyclerView) findViewById(R.id.my_recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        currentTag = Config.SEARCH_TAG;
        currentLat = "";
        currentLon = "";
        currentSearchType = "TAG_SEARCH";
        currentUserName = getExtraString(savedInstanceState,"username");
        currentUserId = getExtraString(savedInstanceState,"userid");

       // Log.d()

        final IProfile profile;

            profile = new ProfileDrawerItem()
                    .withName(currentUserName)
                    .withEmail(currentUserName+"@yahoo.com");
                   // .withIcon();

        Glide
                .with(getApplicationContext())
                .load(String.format(Config.BUDDY_ICON_URL,currentUserId))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(100,100) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        profile.withIcon(resource);
                        headerResult.addProfiles(profile);
                    }
                });

        bottomLayout = (RelativeLayout)findViewById(R.id.loadItemsLayout_recyclerView);

        initSwipeRefreshLayout();
        initPhotos("","","",Config.FETCH_RECENT,pages,"TAG_SEARCH");
        initSearchView();
        initPopularTags();
        initShakeDetector();
    }

    private void initPopularTags() {
        String api_url = String.format(Config.API_URL_TAG, Config.FETCH_POPULAR_TAGS, Config.FLICKR_KEY,"","","","");
        new DownloadJsonTask(new DownloadJsonTask.AsyncResponse() {
            @Override
            public void onProcessFinished(JSONObject result) {
                try{
                    JSONArray photosJson = result.getJSONObject("hottags").getJSONArray("tag");
                    JSONObject currentTag;
                    for(int i=0; i<photosJson.length(); i++){
                        currentTag = photosJson.getJSONObject(i);
                        popularTags.add(currentTag.getString("_content"));
                    }
                }catch(Exception e){
                    Log.e(TAG,e.toString());
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,api_url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu_main; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle toolbar item clicks here. It'll
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                // Open the search view on the menu_main item click.
                searchView.openSearch();
                return true;
            case R.id.location_search:
                LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
                Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                double longitude = location.getLongitude();
                double latitude = location.getLatitude();

                currentLat = longitude+"";
                currentLon = latitude+"";

                photosList.clear();
                photosList.trimToSize();
                currentTag = "";
                pages = 1;
                initPhotos(currentTag,currentLat,currentLon,Config.FETCH_IMAGE,pages,"LOCATION_SEARCH");
                currentSearchType="LOCATION_SEARCH";
                toolbar.setTitle("#currentLocation");

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (searchView.isOpen()) {
            // Close the search on the back button press.
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        searchView.clearSuggestions();
        mSensorManager.unregisterListener(mShakeDetector);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        searchView.activityResumed();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }
    private void setGridLayoutManager() {
        recyclerViewMain.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 2);
        recyclerViewMain.setLayoutManager(mLayoutManager);
    }

    private void setAdapter() {
        adapter = new GridLayoutAdapter(this, photosList);
        recyclerViewMain.setAdapter(adapter);
        adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, DisplayPhotoActivityViewPager.class);
                intent.putExtra("itemClicked",i);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRefresh() {
        photosList.clear();
        adapter.notifyDataSetChanged();
        photosList.trimToSize();
        pages=1;

        if(currentTag.equalsIgnoreCase("recent"))
            initPhotos("",currentLat,currentLon,Config.FETCH_RECENT,pages,"TAG_SEARCH");
        else
            initPhotos(currentTag,currentLat,currentLon,Config.FETCH_IMAGE,pages,currentSearchType);
    }

    private void initSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerViewMain.addOnScrollListener(new OnVerticalScrollListener() {
            @Override
            public void onScrolledToBottom() {
                super.onScrolledToBottom();
                //progressDialog = ProgressDialog.show(MainActivity.this, "Loading", "Loading new photos..", true);
                bottomLayout.setVisibility(View.VISIBLE);
                bottomLayout.bringToFront();
                pages++;

              //  String api_url = String.format(Config.API_URL, Config.FETCH_IMAGE, Config.FLICKR_KEY,currentTag,String.valueOf(pages),currentLat,currentLon);
                String api_url = "";
                if(currentSearchType.equalsIgnoreCase("TAG_SEARCH"))
                    api_url = String.format(Config.API_URL_TAG, Config.FETCH_IMAGE, Config.FLICKR_KEY,currentTag,String.valueOf(pages));
                else if(currentSearchType.equalsIgnoreCase("LOCATION_SEARCH"))
                    api_url = String.format(Config.API_URL_LOCATION,Config.FETCH_IMAGE,Config.FLICKR_KEY,String.valueOf(pages),currentLat,currentLon);

                Log.i("api_url",api_url);
                if(!isLoadingNewItems)
                new DownloadJsonTask(new DownloadJsonTask.AsyncResponse() {
                    @Override
                    public void onProcessFinished(JSONObject result) {
                        try{
                            JSONArray photosJson = result.getJSONObject("photos").getJSONArray("photo");
                            JSONObject currentPhoto;
                            for(int i=0; i<photosJson.length(); i++){
                                currentPhoto = photosJson.getJSONObject(i);
                                photosList.add(new Photo(
                                        currentPhoto.getString("id"),photoKeyGenerator.getAndIncrement(),currentPhoto.getString("owner"),
                                        currentPhoto.getString("secret"),currentPhoto.getString("server"),
                                        currentPhoto.getString("farm"), currentPhoto.getString("title")));
                            }
                        }catch(Exception e){
                            Log.e(TAG,e.toString());
                        }

                        adapter.notifyDataSetChanged();
                        //progressDialog.dismiss();
                        isLoadingNewItems = false;
                        bottomLayout.setVisibility(View.GONE);
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,api_url);
                isLoadingNewItems = true;
                //swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onScrolledToTop() {
                super.onScrolledToTop();
            }
        });
    }

    private void initShakeDetector() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {
            @Override
            public void onShake(int count) {
                photosList.clear();
                adapter.notifyDataSetChanged();
                photosList.trimToSize();
                swipeRefreshLayout.setRefreshing(true);
                currentTag = popularTags.get(new Random().nextInt(popularTags.size()));
                pages = 1;
                initPhotos(currentTag,"","",Config.FETCH_IMAGE,pages,"TAG_SEARCH");
            }
        });
    }

    private void initPhotos(String tag,String lat, String lon,String method,int page,String type) {
        String api_url = "";
        if(type.equalsIgnoreCase("TAG_SEARCH")) {
            api_url = String.format(Config.API_URL_TAG, method, Config.FLICKR_KEY, tag, page);
            super.getSupportActionBar().setTitle("#"+currentTag);
        }
        else if(type.equalsIgnoreCase("LOCATION_SEARCH")) {
            api_url = String.format(Config.API_URL_LOCATION, method, Config.FLICKR_KEY, page, lat, lon);
           // super.getSupportActionBar().setTitle("#"+current);
        }

        Log.i("api_url",api_url);
        new DownloadJsonTask(new DownloadJsonTask.AsyncResponse() {

            @Override
            public void onProcessFinished(JSONObject result) {
                try{
                    JSONArray photosJson = result.getJSONObject("photos").getJSONArray("photo");
                    JSONObject currentPhoto;
                    for(int i=0; i<photosJson.length(); i++){
                        currentPhoto = photosJson.getJSONObject(i);
                        photosList.add(new Photo(
                                currentPhoto.getString("id"),photoKeyGenerator.getAndIncrement(),currentPhoto.getString("owner"),
                                currentPhoto.getString("secret"),currentPhoto.getString("server"),
                                currentPhoto.getString("farm"), currentPhoto.getString("title")));
                    }
                }catch(Exception e){
                    Log.e(TAG,e.toString());
                }

                if(recyclerViewMain.getLayoutManager()==null)
                    setGridLayoutManager();
                setAdapter();

                initPhotoDetails();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,api_url);

        //swipeRefreshLayout.setRefreshing(false);
      //  setSupportActionBar(toolbar);

    }

    private void initPhotoDetails() {
        Log.d("XMLParser: " , "started");
        new XMLParsePhotoDetailsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void initSearchView(){
        searchView.setOnQueryTextListener(new CustomMaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                photosList.clear();
                photosList.trimToSize();
                currentTag = query;
                currentLat = "";
                currentLon="";
                pages = 1;
                currentSearchType="TAG_SEARCH";
                initPhotos(currentTag,currentLat,currentLon,Config.FETCH_IMAGE,pages,"TAG_SEARCH");
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {

                if(newText==null || newText.length()==0) {
                    searchView.removeAllPlaces();
                    return false;
                }

                if(newText.length()==1){
                    oldSearchText = newText;
                    return false;
                }
                else{
                    if (newText.length() > 1 && !newText.equalsIgnoreCase(oldSearchText)){
                        updateList(newText);
                        Log.d(TAG+" text: ",newText);
                    }
                }
                return false;
            }
        });

        searchView.setSearchViewListener(new CustomMaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewOpened() {
                // Do something once the view is open.
            }

            @Override
            public void onSearchViewClosed() {
                // Do something once the view is closed.
            }
        });

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Do something when the suggestion list is clicked.
                TextView tv = (TextView) view.findViewById(R.id.tv_str);
                final Place currentClickedPlace;

                if (tv != null) {
                    searchView.setQuery(tv.getText().toString(),false);
                    Toast.makeText(MainActivity.this, places.get(position).getDescription(), Toast.LENGTH_SHORT).show();
                    currentClickedPlace = places.get(position);
                    new DownloadJsonTask(new DownloadJsonTask.AsyncResponse() {
                        @Override
                        public void onProcessFinished(JSONObject result) {
                            try {
                                searchView.closeSearch();
                                String lat;
                                String lon;
                                Log.d(TAG+" json ",result.toString());

                                //Log.d(TAG+"add_comp: " , ja.length()+"");
                                JSONObject locations = result
                                        .getJSONObject("result")
                                        .getJSONObject("geometry")
                                        .getJSONObject("location");
                                //JSONObject geoMetryObject = results.getJSONObject("geometry");
                                //JSONObject locations = geoMetryObject.getJSONObject("location");

                             //   Toast.makeText(MainActivity.this, "Lat: "+currentLat+" Lon: " +currentLon, Toast.LENGTH_SHORT).show();

                            } catch (Exception e) {
                            }
                        }
                    }).execute(currentClickedPlace.getDetailDownloadUrl());
                }
            }
        });
        searchView.adjustTintAlpha(0.8f);
    }

    public String getExtraString(Bundle savedInstanceState, String key) {
        String result;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                result = "";
            } else {
                result = extras.getString(key);
            }
        } else {
            result = (String) savedInstanceState.getSerializable(key);
        }
        return result;
    }

    public void updateList(String place) {

      //searchView.clearAll();
        String input ="";
        try {
            input=URLEncoder.encode(place,"utf-8");

            String url = String.format(Config.PLACES_AUTOCOMPLETE_URL,input,Config.GOOGLE_PLACES_SERVER_KEY);
            Log.d(TAG+" url: ",url);
            oldSearchText = place;
            if(true)
            new DownloadJsonTask(new DownloadJsonTask.AsyncResponse() {

                @Override
                public void onProcessFinished(JSONObject result) {
                    try {
                        JSONArray ja = result.getJSONArray("predictions");
                        Log.d(TAG+"ja: " , ja.length()+"");
                        places.clear();
                        places.trimToSize();
                        //TODO clearSuggestions also(try)
                        //searchView.clearSuggestions();
                        searchView.removeAllPlaces();

                        for (int i = 0; i < ja.length(); i++) {
                            JSONObject c = ja.getJSONObject(i);
                            String description = c.getString("description");
                            String id = c.getString("place_id");
                            Log.d("description", description);
                            searchView.addPlaces(description);
                            places.add(new Place(id,description));
                        }
                        //searchView.addPlaces(names);
                    } catch (Exception e) {
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }
}
