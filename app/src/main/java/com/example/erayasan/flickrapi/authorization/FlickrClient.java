package com.example.erayasan.flickrapi.authorization;

import android.content.Context;

import org.scribe.builder.api.Api;
import org.scribe.builder.api.FlickrApi;

public class FlickrClient extends OAuthBaseClient {
    public static final Class<? extends Api> REST_API_CLASS = FlickrApi.class;
    public static final String REST_URL = "http://www.flickr.com/services";
    public static final String REST_CONSUMER_KEY = "57ac210e2e82195e071f9a761d763ca0";
    public static final String REST_CONSUMER_SECRET = "7d359e4f4149545b";
    public static final String REST_CALLBACK_URL = "oauth://cprest";
    public final static String FLICKR_KEY = "7f93b4f229b8a47970bb88d25c8f4592";
    
    public FlickrClient(Context context) {
        super(context, REST_API_CLASS, REST_URL, REST_CONSUMER_KEY, REST_CONSUMER_SECRET, REST_CALLBACK_URL);
        setBaseUrl("https://api.flickr.com/services/rest");
    }

    @Override
    public void connect() {
        super.connect();
    }

}
