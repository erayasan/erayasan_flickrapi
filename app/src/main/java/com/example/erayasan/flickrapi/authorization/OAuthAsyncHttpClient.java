//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.erayasan.flickrapi.authorization;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.codepath.utils.AsyncSimpleTask;
import com.codepath.utils.AsyncSimpleTask.AsyncSimpleTaskHandler;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.Api;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class OAuthAsyncHttpClient extends AsyncHttpClient {
    private OAuthAsyncHttpClient.OAuthTokenHandler handler;
    private Token accessToken;
    public static OAuthService service;

    public OAuthAsyncHttpClient(Class<? extends Api> apiClass, String consumerKey, String consumerSecret, String callbackUrl, OAuthAsyncHttpClient.OAuthTokenHandler handler) {
        this.handler = handler;
        if(callbackUrl == null) {
            callbackUrl = "oob";
        }
        this.service = (new ServiceBuilder()).provider(apiClass).apiKey(consumerKey).apiSecret(consumerSecret).callback(callbackUrl).build();
    }

    public void fetchRequestToken() {
        new AsyncSimpleTask(new AsyncSimpleTaskHandler() {
            String authorizeUrl = null;
            Exception e = null;
            Token requestToken;

            public void doInBackground() {
                try {
                    this.requestToken = OAuthAsyncHttpClient.this.service.getRequestToken();
                    this.authorizeUrl = OAuthAsyncHttpClient.this.service.getAuthorizationUrl(this.requestToken);
                } catch (Exception var2) {
                    this.e = var2;
                }
            }

            public void onPostExecute() {
                if(this.e != null) {
                    OAuthAsyncHttpClient.this.handler.onFailure(this.e);
                } else {
                    OAuthAsyncHttpClient.this.handler.onReceivedRequestToken(this.requestToken, this.authorizeUrl);
                }
            }
        });
    }

    public void fetchAccessToken(final Token requestToken, final Uri uri) {
        new AsyncSimpleTask(new AsyncSimpleTaskHandler() {
            Exception e = null;

            public void doInBackground() {
                Uri authorizedUri = uri;
                String oauth_verifier = authorizedUri.getQueryParameter("oauth_verifier");
                Log.d("DEBUG oauth_verifier",oauth_verifier);
                Verifier verifier = new Verifier(oauth_verifier);
                Log.d("DEBUG verifier(value)",verifier.getValue());

                try {
                    OAuthAsyncHttpClient.this.accessToken = OAuthAsyncHttpClient.this.service.getAccessToken(requestToken, verifier);
                    Log.d("DEBUG url: ",OAuthAsyncHttpClient.this.service.getAuthorizationUrl(requestToken));
                } catch (Exception var4) {
                    this.e = var4;
                }

            }

            public void onPostExecute() {
                if(this.e != null) {
                    OAuthAsyncHttpClient.this.handler.onFailure(this.e);
                } else {
                    OAuthAsyncHttpClient.this.setAccessToken(OAuthAsyncHttpClient.this.accessToken);
                    OAuthAsyncHttpClient.this.handler.onReceivedAccessToken(OAuthAsyncHttpClient.this.accessToken);
                }

            }
        });
    }

    public void setAccessToken(Token accessToken) {
        if(accessToken == null) {
            this.accessToken = null;
        } else {
            this.accessToken = accessToken;

        }

    }

    protected void sendRequest(DefaultHttpClient client, HttpContext httpContext, HttpUriRequest uriRequest, String contentType, AsyncHttpResponseHandler responseHandler, Context context) {
        if(this.service != null) {
            try {
                ScribeRequestAdapter e = new ScribeRequestAdapter(uriRequest);
                this.service.signRequest(this.accessToken, e);
                super.sendRequest(client, httpContext, uriRequest, contentType, responseHandler, context);
            } catch (Exception var8) {
                var8.printStackTrace();
            }
        }
    }

    public interface OAuthTokenHandler {
        void onReceivedRequestToken(Token var1, String var2);

        void onReceivedAccessToken(Token var1);

        void onFailure(Exception var1);
    }
}
