package com.example.erayasan.flickrapi.utils;

public class Config {

    public final static String FLICKR_KEY = "e45dc9538a1499e1ab40695cb181c2d9";
    public final static String FLICKR_SECRET = "790d5b1fe96d7308";
    public final static String GOOGLE_PLACES_SERVER_KEY = "AIzaSyCT1-eIzrHDm6JLfU-JCwvczw9mHz8Gi_M";

    public final static int MEMORY_CACHE_SIZE = 5 * 1024 * 1024; // 5mb

     public final static String API_URL_TAG = "https://api.flickr.com/services/rest/?method=%s&api_key=%s&tags=%s&page=%s&format=json&nojsoncallback=1";
    public final static String API_URL_LOCATION = "https://api.flickr.com/services/rest/?method=%s&api_key=%s&page=%s&lat=%s&lon=%s&format=json&nojsoncallback=1";
    public final static String API_URL_PHOTO_DETAILS = "https://api.flickr.com/services/rest/?method=%s&api_key=%s&photo_id=%s&secret=%s";

    public final static String PLACES_DETAIL_URL = "https://maps.googleapis.com/maps/api/place/details/json?placeid=%s&key=%s";
    public final static String PLACES_AUTOCOMPLETE_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?&types=geocode&input=%s&key=%s";

   public final static String FETCH_IMAGE = "flickr.photos.search";
    public final static String FETCH_POPULAR_TAGS = "flickr.tags.getHotList";
    public final static String FETCH_RECENT = "flickr.photos.getRecent";
    public final static String FETCH_IMAGE_DETAILS = "flickr.photos.getInfo";

    public final static String SEARCH_TAG = "recent";

    /* http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg */
    public final static String IMAGE_URL = "http://farm%s.staticflickr.com/%s/%s_%s_%s.jpg";
    public final static String BUDDY_ICON_URL = "http://www.flickr.com/buddyicons/%s.jpg";

    public final static String IMAGE_SIZE_SMALL_SQUARE = "s";
    public final static String IMAGE_SIZE_LARGE_SQUARE = "q";
    public final static String IMAGE_SIZE_SMALL = "m";
    public final static String IMAGE_SIZE_MEDIUM = "z";
    public final static String IMAGE_SIZE_LARGE = "b";
    public final static String IMAGE_SIZE_VERY_LARGE = "h";

}

