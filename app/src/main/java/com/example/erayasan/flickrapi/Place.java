package com.example.erayasan.flickrapi;

import com.example.erayasan.flickrapi.utils.Config;

/**
 * Created by erayasan on 12/07/16.
 */
public class Place {
    private String latitude;
    private String longtitude;
    private String description;
    private String placeId;

    public Place(String latitude, String longtitude, String description) {
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.description = description;
    }

    public Place(String placeId, String description) {
        this.placeId = placeId;
        this.description = description;
    }

    public String getPlaceId(){ return placeId;}

    public void setPlaceId(String placeId){this.placeId = placeId;}

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetailDownloadUrl(){
        return String.format(Config.PLACES_DETAIL_URL, placeId,Config.GOOGLE_PLACES_SERVER_KEY);
    }
}
