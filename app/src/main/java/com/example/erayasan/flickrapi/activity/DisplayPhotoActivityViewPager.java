package com.example.erayasan.flickrapi.activity;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.WindowManager;

import com.example.erayasan.flickrapi.R;
import com.example.erayasan.flickrapi.adapter.SimplePagerAdapter;
import com.example.erayasan.flickrapi.utils.HackyViewPager;

public class DisplayPhotoActivityViewPager extends AppCompatActivity {

    private ViewPager mViewPager;
    private int screenWidth;
    private int currentPhotoPosition;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        mViewPager = (HackyViewPager) findViewById(R.id.view_pager);

        //setContentView(mViewPager);

        WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;

        int itemClicked = getExtraInteger(savedInstanceState, "itemClicked");
        currentPhotoPosition = itemClicked;

        mViewPager.setAdapter(new SimplePagerAdapter(DisplayPhotoActivityViewPager.this,screenWidth));
        mViewPager.setCurrentItem(currentPhotoPosition);

    }

    public int getExtraInteger(Bundle savedInstanceState, String key) {
        int result;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                result = -1;
            } else {
                result = extras.getInt(key);
            }
        } else {
            result = (int) savedInstanceState.getSerializable(key);
        }
        return result;
    }

}