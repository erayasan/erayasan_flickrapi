//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.erayasan.flickrapi.authorization;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;

import org.scribe.builder.api.Api;
import org.scribe.model.Token;

public abstract class OAuthBaseClient {
    protected String baseUrl;
    protected Context context;
    protected OAuthAsyncHttpClient client;
    protected SharedPreferences prefs;
    protected Editor editor;
    protected OAuthBaseClient.OAuthAccessHandler accessHandler;
    public static OAuthBaseClient instance;

    public static OAuthBaseClient getInstance(Class<? extends OAuthBaseClient> klass, Context context) {
        if(instance == null) {
            try {
                instance = (OAuthBaseClient)klass.getConstructor(new Class[]{Context.class}).newInstance(new Object[]{context});
            } catch (Exception var3) {
                var3.printStackTrace();
            }
        }
        return instance;
    }

    public OAuthBaseClient(Context c, Class<? extends Api> apiClass, String consumerUrl, String consumerKey, String consumerSecret, String callbackUrl) {
        this.baseUrl = consumerUrl;
        this.client = new OAuthAsyncHttpClient(apiClass, consumerKey, consumerSecret, callbackUrl, new OAuthAsyncHttpClient.OAuthTokenHandler() {

            public void onReceivedRequestToken(Token requestToken, String authorizeUrl) {
                OAuthBaseClient.this.editor.putString("request_token", requestToken.getToken());
                OAuthBaseClient.this.editor.putString("request_token_secret", requestToken.getSecret());
                OAuthBaseClient.this.editor.commit();
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(authorizeUrl + "&perms=delete"));
                OAuthBaseClient.this.context.startActivity(intent);
            }

            public void onReceivedAccessToken(Token accessToken) {
                OAuthBaseClient.this.client.setAccessToken(accessToken);
                OAuthBaseClient.this.editor.putString("oauth_token", accessToken.getToken());
                OAuthBaseClient.this.editor.putString("oauth_token_secret", accessToken.getSecret());
                OAuthBaseClient.this.editor.commit();
                OAuthBaseClient.this.accessHandler.onLoginSuccess();
            }

            public void onFailure(Exception e) {
                OAuthBaseClient.this.accessHandler.onLoginFailure(e);
            }
        });
        this.context = c;
        this.prefs = this.context.getSharedPreferences("OAuth", 0);
        this.editor = this.prefs.edit();
        if(this.checkAccessToken() != null) {
            this.client.setAccessToken(this.checkAccessToken());
        }

    }

    public void connect() {
        this.client.fetchRequestToken();
    }

    public void authorize(Uri uri, OAuthBaseClient.OAuthAccessHandler handler) {
        this.accessHandler = handler;
        if(this.checkAccessToken() == null && uri != null) {
            this.client.fetchAccessToken(this.getRequestToken(), uri);
        } else if(this.checkAccessToken() != null) {
            this.accessHandler.onLoginSuccess();
        }

    }

    public Token checkAccessToken() {
        return this.prefs.contains("oauth_token") && this.prefs.contains("oauth_token_secret")?new Token(this.prefs.getString("oauth_token", ""), this.prefs.getString("oauth_token_secret", "")):null;
    }

    protected Token getRequestToken() {
        return new Token(this.prefs.getString("request_token", ""), this.prefs.getString("request_token_secret", ""));
    }

    protected void setBaseUrl(String url) {
        this.baseUrl = url;
    }

    protected String getApiUrl(String path) {
        return this.baseUrl + "/" + path;
    }

    public void clearAccessToken() {
        this.client.setAccessToken((Token)null);
        this.editor.remove("oauth_token");
        this.editor.remove("oauth_token_secret");
        this.editor.commit();
    }

    public interface OAuthAccessHandler {
        void onLoginSuccess();
        void onLoginFailure(Exception var1);
    }
}
