package com.example.erayasan.flickrapi;

import com.example.erayasan.flickrapi.utils.Config;

/**
 * Created by erayasan on 22/06/16.
 */
public class Photo {

    private String id;
    private String owner;
    private String secret;
    private String server;
    private String farm;
    private String title;
    private Integer bitmapKey; // key to find corrosponding bitmap from the cache
    private String description;
    private String takenDate;
    private String flickrUrl;
    private String location;


    public Photo(String id, int bitmapKey, String owner, String secret, String server, String farm, String title) {
        this.id = id;
        this.bitmapKey = bitmapKey;
        this.owner = owner;
        this.secret = secret;
        this.server = server;
        this.farm = farm;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getFarm() {
        return farm;
    }

    public void setFarmNo(String farm) {
        this.farm = farm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public Integer getBitmapKey() {
        return bitmapKey;
    }

    public void setBitmapKey(Integer bitmapKey) {
        this.bitmapKey = bitmapKey;
    }

    public String getImageUrl(String size){
        return  String.format(Config.IMAGE_URL, farm, server, id, secret,size);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDate(String date) {
        this.takenDate = date;
    }

    public String getDate(){
        return this.takenDate;
    }

    public void setFlickrUrl(String flickrUrl) {
        this.flickrUrl = flickrUrl;
    }

    public String getFlickrUrl(){
        return this.flickrUrl;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    public String getLocation() {
        return this.location;
    }
}
