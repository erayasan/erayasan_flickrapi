package com.example.erayasan.flickrapi.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.erayasan.flickrapi.Photo;
import com.example.erayasan.flickrapi.R;
import com.example.erayasan.flickrapi.activity.MainActivity;
import com.example.erayasan.flickrapi.utils.Config;

import java.util.ArrayList;

public class GridLayoutAdapter extends CustomRecyclerViewAdapter {
    private Activity activity;
    private ArrayList<Photo> photos;
    private int screenWidth;
    int count = 0;

    public GridLayoutAdapter(Activity activity, ArrayList<Photo> photos) {
        this.activity = activity;
        this.photos = photos;

        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
    }

    @Override
    public CustomRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity)
                .inflate(R.layout.grid_item, parent, false);
        Holder dataObjectHolder = new Holder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CustomRecycleViewHolder holder, final int position) {
        final Holder myHolder = (Holder) holder;

        Glide.with(activity)
                .load(photos.get(position).getImageUrl(Config.IMAGE_SIZE_LARGE_SQUARE))
                .error(R.drawable.loading_error)
                .placeholder(R.drawable.loading)
                .override(screenWidth / 2, 500)
                .centerCrop().into((myHolder.images));
        count++;
        if(count==8) // 8 is the max number of photos that can be displayed in the screen.
            MainActivity.swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public class Holder extends CustomRecycleViewHolder {
        private ImageView images;

        public Holder(View itemView) {
            super(itemView);
            images = (ImageView) itemView.findViewById(R.id.ivItemGridImage);
        }
    }
}
